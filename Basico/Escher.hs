module Basico.Escher where
import Dibujo
import Interp
import Graphics.Gloss.Data.Picture



type Escher = Bool

-- Muestra la figura formada(gracias a Main.hs) 
figura = escher 2 True

interpBas :: Output Escher
interpBas True = trian2
interpBas False = simple blank

-- Dibujo u
dibujo_u :: Dibujo Escher -> Dibujo Escher
dibujo_u p = encimar4 p

-- Dibujo t 
dibujo_t :: Dibujo Escher -> Dibujo Escher
dibujo_t p = p ^^^ (a ^^^ b) 
    where a = Rot45 p
          b = r270 (Espejar a)
  
blanco = Basica False

-- Esquina con nivel de detalle en base a la figura p
esquina :: Int -> Dibujo Escher -> Dibujo Escher
esquina 0 p = blanco
esquina n p = cuarteto (esquina (n-1) p) (lado (n-1) (dibujo_t p)) (Rotar(lado (n-1) (dibujo_t p))) (dibujo_u p)

-- Lado con nivel de detalle.
lado :: Int -> Dibujo Escher -> Dibujo Escher
lado 0 p = blanco
lado n p = cuarteto (lado (n-1) p) (lado (n-1) p) (Rotar p) p

-- Construimos un "esqueleto" de nueve baldosas.
noneto p q r s t u v w x = Apilar 1 2 (Juntar 1 2 p (q /// r)) ((Juntar 1 2 s (t /// u)) .-. (Juntar 1 2 v (w /// x)))
  
-- Construimos la figura final para mostrar.
escher :: Int -> Escher -> Dibujo Escher
escher n p = noneto (esquina n p') (lado n (dibujo_t p')) (r270 (esquina n p')) 
             (Rotar (lado n (dibujo_t p'))) (dibujo_u p') (r270 (lado n (dibujo_t p'))) 
             (Rotar (esquina n p')) (r180 (lado n (dibujo_t p'))) (r180 (esquina n p'))
              where p' = Basica p

