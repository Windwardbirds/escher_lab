module Dibujo where
import Data.Either

--------------- Definición del lenguaje como un tipo de datos --------------------

data Dibujo a = Basica a | Rotar (Dibujo a) | Espejar (Dibujo a) | Rot45 (Dibujo a)
     | Apilar Int Int (Dibujo a) (Dibujo a)
     | Juntar Int Int (Dibujo a) (Dibujo a)
     | Encimar (Dibujo a) (Dibujo a)
     deriving Show

-------------------------- Definición de combinadores ---------------------------


-- Composición n-veces de una función con sí misma.
comp :: (a -> a) -> Int -> a -> a
comp funcion 0 a = a
comp funcion n a = comp funcion (n-1) (funcion(a))

-- Rotaciones de múltiplos de 90.
r180 :: Dibujo a -> Dibujo a
r180 a = comp Rotar 2 a

r270 :: Dibujo a -> Dibujo a
r270 a = comp Rotar 3 a

-- Pone una figura sobre la otra, ambas ocupan el mismo espacio.
(.-.) :: Dibujo a -> Dibujo a -> Dibujo a
a .-. b = Apilar 1 1 a b --Pendiente

-- Pone una figura al lado de la otra, ambas ocupan el mismo espacio.
(///) :: Dibujo a -> Dibujo a -> Dibujo a
a /// b = Juntar 1 1 a b

-- Superpone una figura con otra.
(^^^) :: Dibujo a -> Dibujo a -> Dibujo a
a ^^^ b = Encimar a b

-- Dadas cuatro figuras, las coloca en los cuatro cuadrantes.
cuarteto :: Dibujo a -> Dibujo a  -> Dibujo a -> Dibujo a -> Dibujo a
cuarteto a b c d = (a /// b) .-. (c /// d)

-- Una figura repetida con las cuatro rotaciones, superpuestas.
encimar4 :: Dibujo a -> Dibujo a
encimar4 a = ((a ^^^ (Rotar a)) ^^^ (r180 a)) ^^^ (r270 a)

-- Dada una figura, la rota tres veces y coloca cada rotación (y la original) en cada cuadrante.
ciclar :: Dibujo a -> Dibujo a
ciclar a = cuarteto a (Rotar a) (r180 a) (r270 a)

------------------ Esquema para manipulacion de figuras --------------------------

-- Ver un a como una figura.
pureDibe :: a -> Dibujo a
pureDibe a = Basica a

-- Implementación de map para nuestro lenguaje.
mapDib :: (a -> b) -> Dibujo a -> Dibujo b
mapDib funcion (Basica a) = Basica (funcion a)
mapDib funcion (Rotar a) = Rotar (mapDib funcion a)
mapDib funcion (Espejar a) = Espejar (mapDib funcion a)
mapDib funcion (Rot45 a) = Rot45 (mapDib funcion a)
mapDib funcion (Encimar a b) = Encimar (mapDib funcion a) (mapDib funcion b)
mapDib funcion (Juntar x y d d') = Juntar x y (mapDib funcion d) (mapDib funcion d')
mapDib funcion (Apilar x y d d') = Apilar x y (mapDib funcion d) (mapDib funcion d')

-- convencerse que se satisface
-- 1. cambiar pureDibe = id
-- 2. cambiar f (pureDibe a) = f a
-- 3. (cambiar g) (cambiar f ma) = cambiar (cambiar g . f) ma
cambia :: (a -> Dibujo b) -> Dibujo a -> Dibujo b
cambia funcion (Basica a) = funcion a
cambia funcion (Rotar a) = Rotar (cambia funcion a)
cambia funcion (Espejar a) = Espejar (cambia funcion a)
cambia funcion (Rot45 a) = Rot45 (cambia funcion a)
cambia funcion (Encimar a b) = Encimar (cambia funcion a) (cambia funcion b)
cambia funcion (Juntar x y d d') = Juntar x y (cambia funcion d) (cambia funcion d')
cambia funcion (Apilar x y d d') = Apilar x y (cambia funcion d) (cambia funcion d')

-- Estructura general para la semántica.
sem :: (a -> b) -> (b -> b) -> (b -> b) -> (b -> b) ->
       (Int -> Int -> b -> b -> b) -> 
       (Int -> Int -> b -> b -> b) -> 
       (b -> b -> b) ->
       Dibujo a -> b
sem bas rot esp r45 apl jnt enc (Basica a) = bas a
sem bas rot esp r45 apl jnt enc (Rotar a) = rot (sem bas rot esp r45 apl jnt enc a)
sem bas rot esp r45 apl jnt enc (Espejar a) = esp (sem bas rot esp r45 apl jnt enc a)
sem bas rot esp r45 apl jnt enc (Rot45 a) = r45 (sem bas rot esp r45 apl jnt enc a)
sem bas rot esp r45 apl jnt enc (Encimar a b) = enc (sem bas rot esp r45 apl jnt enc a) (sem bas rot esp r45 apl jnt enc b)
sem bas rot esp r45 apl jnt enc (Juntar x y d d') = jnt x y (sem bas rot esp r45 apl jnt enc d) (sem bas rot esp r45 apl jnt enc d')
sem bas rot esp r45 apl jnt enc (Apilar x y d d') = apl x y (sem bas rot esp r45 apl jnt enc d) (sem bas rot esp r45 apl jnt enc d')

--------------------------- Otras funciones --------------------------------

type Pred a = a -> Bool

-- Dado un predicado sobre básicas, cambia todas las que satisfacen
-- el predicado por una figura vacía.
limpia :: Pred a -> a -> Dibujo a -> Dibujo a
limpia pred b a = mapDib (\d -> if pred d then b else d) a

-- Comprueba si alguna básica satisface el predicado.
anyDib :: Pred a -> Dibujo a -> Bool
anyDib pred a = sem (pred) id id id  (\a b c p -> c || p) (\a b c p -> c || p) (\c p -> c || p) a

-- Comprueba que todas las básicas satisfacen el predicado.
allDib :: Pred a -> Dibujo a -> Bool
allDib pred a = sem (pred) id id id  (\a b c p -> c && p) (\a b c p -> c && p) (\c p -> c && p) a

-- Describe la figura. Ejemplos: 
--   desc (Basica b) (const "b") = "b"
--   desc (Rotar fa) db = "rot (" ++ desc fa db ++ ")"
-- (La descripción de cada constructor son sus tres primeros
-- símbolos en minúscula.)
desc :: Show a => (a -> String) -> Dibujo a -> String
desc fun a = sem (fun) (\a -> "rot (" ++ a ++ ")") (\a -> "esp (" ++ a ++ ")") (\a -> "r45 (" ++ a ++ ")")  (\a b c p -> "apl (" ++ c ++ ") (" ++ p ++ ")") (\a b c p -> "jnt (" ++ c ++ ") (" ++ p ++ ")") (\a b -> "enc (" ++ a ++ ") (" ++ b ++ ")") a
--desc (show) (Juntar 1 1 (Rotar (Basica 4)) (Basica 9))
--"jnt (rot (4)) (9)"

-- Junta todas las figuras básicas de un dibujo.
every :: Dibujo a -> [a]
every a = sem (\a -> a : []) id id id  (\a b c p -> c ++ p) (\a b c p -> c ++ p) (\c p -> c ++ p) a


-- Cuenta cuantas veces una tupla aparece en una lista de tuplas.
contarTuplas :: Eq a => [(a, Int)] -> (a, Int) -> [(a, Int)]
contarTuplas [] b = [b]
contarTuplas (x:xs) b | fst x == fst b = [(fst x, snd x + snd b)]
                      | otherwise = contarTuplas xs b

borrarTupla :: Eq a => (a, Int) -> [(a, Int)] -> [(a, Int)]
borrarTupla d xs = filter (\x -> fst(x) /= fst(d)) xs

--Toma dos listas de tuplas y las combina después de modificarlas con la función contarTuplas y borrarTupla.
contarEncimar :: Eq a => [(a, Int)] -> [(a, Int)]
contarEncimar [] = []
contarEncimar (x:xs) = contarTuplas xs x ++ contarEncimar (borrarTupla x xs)


-- Cuenta la cantidad de veces que aparecen las básicas distintas en una figura.
contar :: Eq a => Dibujo a -> [(a,Int)]
contar a = sem (\a -> [(a, 1)]) id id id  (\a b c p -> contarEncimar (c ++ p)) (\a b c p -> contarEncimar (c ++ p)) (\a b -> contarEncimar (a ++ b)) a

---------------- Definición de predicados sobre figuras. -----------------------

-- Hay 4 rotaciones seguidas (empezando en el tope).
esRot360 :: Pred (Dibujo a)
esRot360 (Rotar (Rotar (Rotar (Rotar a)))) = True
esRot360 a = False

-- Hay 2 espejados seguidos (empezando en el tope).
esFlip2 :: Pred (Dibujo a)
esFlip2 (Espejar (Espejar a)) = True
esFlip2 a = False


-------- Definición de función que aplica un predicado y devuelve -----------------
--------- un error indicando fallo o una figura si no hay el error. ----------------


-- Toma un predicado, un String(que describe el error) y un Dibujo.
-- Devuelve el String si hay error, sino devuelve el Dibujo.
check :: Pred (Dibujo a) -> String -> Dibujo a -> Either String (Dibujo a)
check pred str a | pred a = Left str
                 | otherwise = Right a

  
--Toma un Dibujo y checkea si hay doble espejado.
--Al resultado lo devuelve en una lista que contiene Strings y Dibujos.
reallyok :: Dibujo a -> [Either String (Dibujo a)]
reallyok (Basica a) = [(check (esFlip2) "Doble espejado." (Basica a)), (check (esRot360) "Cuadruple rotar." (Basica a))]
reallyok (Rotar a) = [(check (esFlip2) "Doble espejado." (Rotar a)), (check (esRot360) "Cuadruple rotar." (Rotar a))] ++ reallyok a
reallyok (Espejar a) = [(check (esFlip2) "Doble espejado." (Espejar a)), (check (esRot360) "Cuadruple rotar." (Espejar a))] ++ reallyok a
reallyok (Rot45 a) = [(check (esFlip2) "Doble espejado." (Rot45 a)), (check (esRot360) "Cuadruple rotar." (Rot45 a))] ++ reallyok a
reallyok (Encimar a b) = reallyok b ++ reallyok a
reallyok (Juntar d d' a b) = reallyok b ++ reallyok a
reallyok (Apilar d d' a b) = reallyok b ++ reallyok a

--Toma un Dibujo, lo evalúa con reallyok y lefts, que dan como resultado
--una lista de Strings.
--Si la lista no es vacía, devuelve la lista de Strings.
--Si la lista es vacía, devuelve el Dibujo sin cambios.
todoBien :: Dibujo a -> Either [String] (Dibujo a)
todoBien a | length leftelems /= 0 = Left leftelems
           | otherwise = Right a 
            where evaluation = reallyok a
                  leftelems = lefts evaluation

---------- Definición de funciones que corrigen los errores detectados:-----------------

--Auxilar para definir lo que vamos a cambiar.
cambiar  :: Dibujo a -> Dibujo a
cambiar (Espejar (Espejar a)) = a
cambiar (Rotar (Rotar (Rotar (Rotar a)))) = a
cambiar a = a

--Función recursiva que toma un Dibujo y elimina el doble espejado.
noFlip2 :: Dibujo a -> Dibujo a
noFlip2 (Basica a) = Basica a
noFlip2 (Rotar a) = Rotar (noFlip2 (cambiar a))
noFlip2 (Espejar a) = Espejar (noFlip2 (cambiar a))
noFlip2 (Rot45 a) = Rot45 (noFlip2 (cambiar a))
noFlip2 (Encimar a b) = Encimar (noFlip2 (cambiar a)) (noFlip2 (cambiar b))
noFlip2 (Apilar d d' a b) = Apilar d d' (noFlip2 (cambiar a)) (noFlip2 (cambiar b))
noFlip2 (Juntar d d' a b) = Juntar d d' (noFlip2 (cambiar a)) (noFlip2 (cambiar b))

--Función recursiva que toma un Dibujo y elimina Rotar 360.
noRot360 :: Dibujo a -> Dibujo a
noRot360 (Basica a) = Basica a
noRot360 (Rotar a) = Rotar (noRot360 (cambiar a))
noRot360 (Espejar a) = Espejar (noRot360 (cambiar a))
noRot360 (Rot45 a) = Rot45 (noRot360 (cambiar a))
noRot360 (Encimar a b) = Encimar (noRot360 (cambiar a)) (noRot360 (cambiar b))
noRot360 (Apilar d d' a b) = Apilar d d' (noRot360 (cambiar a)) (noRot360 (cambiar b))
noRot360 (Juntar d d' a b) = Juntar d d' (noRot360 (cambiar a)) (noRot360 (cambiar b))


