# Laboratorio Programación Funcional.

## Descripción

En este laboratorio usamos Haskell para implementar un lenguaje (como un tipo de datos) y es la herramienta que utilizamos para indicar como van colocadas las figuras, que todavía no sabiamos cuales eran (por eso todas las funciones son polimórficas), en una imagen.
Utilizamos la libreria Gloss para darle una interpretacion gráfica.
Las figuras que conforman cada uno de los cuadrantes del noneto en la función Escher fueron implementadas según el [artículo](https://cs.famaf.unc.edu.ar/~mpagano/henderson-funcgeo2.pdf) de Peter Henderson.

## Instalación
```
$ ghc -o Escher Main.hs
$ ./Escher
```
## Librerias

Importamos la librería Data.Either para poder utilizar funciones sobre el tipo de datos Either. En particular lefts filtra los elementos de la forma (left _) de una lista.

## Decisiones de diseño

En Escher.hs definimos la figura vacía (ie. la Picture 'blank') como 'Basica $ False'. La interpretación geométrica de las figuras está dada por la función interpBas, que a su vez define la figura estandar (Basica $ True) como trian2, que es el utilizado en el artículo (ver mas adelante).


